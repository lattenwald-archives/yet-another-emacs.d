(defcustom pm-host/html
  (pm-bchunkmode "html" :mode 'html-mode)
  "HTML chunk host mode"
  :group 'hostmodes
  :type 'object)

(defcustom pm-inner/guile
  (pm-hbtchunkmode "guile"
                   :head-reg "<\\?scm"
                   :tail-reg "\\?>")
  "guile chunk"
  :group 'innermodes
  :type 'object)

(defcustom pm-poly/eguile
  (pm-polymode-one "eguile"
                   :hostmode 'pm-host/html
                   :innermode 'pm-inner/guile)
  "eguile polymode"
  :group 'polymodes
  :type 'object)

(define-polymode poly-html+guile-mode pm-poly/eguile)
