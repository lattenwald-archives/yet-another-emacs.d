(define-package "gist" "1.3.1" "Emacs integration for gist.github.com" '((emacs "24.1") (gh "0.9.2")) :url "https://github.com/defunkt/gist.el" :keywords '("tools"))
