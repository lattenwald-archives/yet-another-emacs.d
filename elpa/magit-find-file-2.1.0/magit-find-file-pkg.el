(define-package "magit-find-file" "2.1.0" "completing-read over all files in Git" '((magit "2.1.0") (dash "2.8.0")) :url "https://github.com/bradleywright/magit-find-file.el" :keywords '("git"))
