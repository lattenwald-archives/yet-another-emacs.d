;;; sunrise-x-loop-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "sunrise-x-loop" "sunrise-x-loop.el" (20957
;;;;;;  6263 100737 967000))
;;; Generated autoloads from sunrise-x-loop.el
 (eval-after-load 'sunrise-commander '(sr-extend-with 'sunrise-x-loop))

;;;***

;;;### (autoloads nil nil ("sunrise-x-loop-pkg.el") (20957 6263 104365
;;;;;;  255000))

;;;***

(provide 'sunrise-x-loop-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; sunrise-x-loop-autoloads.el ends here
